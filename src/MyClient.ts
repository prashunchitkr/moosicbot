import {
  Client,
  ClientOptions,
  Collection,
  Message,
  MessageEmbed,
  TextChannel,
} from "discord.js";
import { Manager } from "erela.js";
import Spotify from "erela.js-spotify";
import { Command } from "./types/common";
import { errorLogger } from "./utils/logger";

export class MyClient extends Client {
  commands: Collection<string, Command>;
  manager: Manager;

  constructor(options: ClientOptions) {
    super(options);
    this.commands = new Collection<string, Command>();
    this.manager = this.initManager();
  }

  private initManager(): Manager {
    const { LAVALINKHOST, LAVALINKPASS, SPOTCLIENTID, SPOTCLIENTSECRET } =
      process.env;

    if (!LAVALINKHOST || !LAVALINKPASS || !SPOTCLIENTID || !SPOTCLIENTSECRET) {
      throw new Error("IMPROPER ENV SETUP");
    }

    const nodes = [{ host: LAVALINKHOST, password: LAVALINKPASS, port: 2333 }];
    return new Manager({
      nodes,
      plugins: [
        new Spotify({
          clientID: SPOTCLIENTID,
          clientSecret: SPOTCLIENTSECRET,
          convertUnresolved: false,
        }),
      ],
      send: (id, payload) => {
        const guild = this.guilds.cache.get(id);
        if (guild) guild.shard.send(payload);
      },
    })
      .on("nodeConnect", (node) =>
        console.log(`Node ${node.options.identifier} connected`)
      )
      .on("nodeError", (node, error) =>
        console.log(
          `Node: ${node.options.identifier} had an error: ${error.message}`
        )
      )
      .on("trackStart", async (player, track) => {
        if (player.textChannel) {
          const channel = this.channels.cache.get(
            player.textChannel
          ) as TextChannel;

          const embed = new MessageEmbed()
            .setTitle("Now Playing")
            .setDescription(
              `[${track.title}](${track.uri}) [${track.requester}]`
            );

          const message = await channel.send({
            embeds: [embed],
          });

          const oldMessage = player.get<Message>("playerMessage");

          if (oldMessage) {
            oldMessage
              .fetch()
              .then((msg) => {
                if (msg) msg.delete();
              })
              .catch((e) => errorLogger.error(e));
          }

          player.set("playerMessage", message);
        }
      })
      .on("trackError", (player, track, payload) => {
        if (payload.error === "No tracks found.") {
          if (player.textChannel) {
            const channel = this.channels.cache.get(
              player.textChannel
            ) as TextChannel;

            channel.send(
              `:negative_squared_cross_mark: No equivalent track found for \`${track.title}\``
            );

            if (
              player.queue.size > 0 &&
              player.queue[0].title === track.title
            ) {
              player.queue.shift();
            }
          }
        }
      })
      .on("queueEnd", (player) => {
        player.get<Message>("playerMessage")?.delete();
        player.set("playerMessage", null);
        setTimeout(() => {
          if (!player.playing) player.destroy();
        }, 1000 * 60 * 5);
      })
      .on("playerMove", async (player, oChannelID, nChannelID) => {
        const nChannel = this.channels.cache.get(nChannelID);
        if (nChannel) {
          console.info("Change voice channel");
          player.setVoiceChannel(nChannelID);
          setTimeout(() => {
            player.pause(false);
          }, 3500);
        }
      });
  }
}
