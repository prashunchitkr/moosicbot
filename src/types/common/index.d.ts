import { SlashCommandBuilder } from "@discordjs/builders";
import {
  Client,
  GuildCommandInteraction,
  PermissionResolvable,
} from "discord.js";
import { Manager } from "erela.js";
import { Collection } from "mongoose";

interface Command {
  requireDJ?: boolean;
  clientPermissions?: PermissionResolvable[];
  data: SlashCommandBuilder;
  execute: (
    client: MyClient,
    interaction: GuildCommandInteraction<"present">
  ) => Promise<void>;
}

interface Event {
  name: string;
  once?: boolean;
  execute: (...any) => void;
}
