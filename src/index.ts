import fs from "fs";
import path from "path";
import { Intents } from "discord.js";
import dotenv from "dotenv";
import { errorLogger } from "./utils/logger";
import { MyClient } from "./MyClient";

dotenv.config();

const { TOKEN, LAVALINKHOST, LAVALINKPASS, SPOTCLIENTID, SPOTCLIENTSECRET } =
  process.env;

if (
  !TOKEN ||
  !LAVALINKHOST ||
  !LAVALINKPASS ||
  !SPOTCLIENTID ||
  !SPOTCLIENTSECRET
) {
  throw new Error("IMPROPER ENV SETUP");
}

export const client = new MyClient({
  intents: [
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.GUILD_MEMBERS,
    Intents.FLAGS.GUILD_PRESENCES,
    Intents.FLAGS.GUILD_EMOJIS_AND_STICKERS,
    Intents.FLAGS.GUILD_MESSAGES,
    Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
    Intents.FLAGS.GUILD_MESSAGE_TYPING,
    Intents.FLAGS.GUILD_VOICE_STATES,
  ],
});

const loadClientEvents = async (dir: string) => {
  const files = fs.readdirSync(path.join(__dirname, dir));

  for (const file of files) {
    import(path.join(__dirname, dir, file))
      .then((f) => f.default)
      .then((event) => {
        if (event.once) {
          client.once(event.name, (...args) => event.execute(...args));
        } else {
          client.on(event.name, (...args) => event.execute(...args));
        }
      });
    console.log(`Registered Event ${file}`);
  }
};

process.on("unhandledRejection", (err) => {
  errorLogger.error(err);
});

loadClientEvents("events/bot");

client.login(TOKEN);
