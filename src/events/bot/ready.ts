import fs from "fs";
import path from "path";

import { client } from "../..";
import { MyClient } from "../../MyClient";
import { Event, Command } from "../../types/common";

const loadCommands = async (dir: string) => {
  const files = fs.readdirSync(path.join(__dirname, dir));

  for (const file of files) {
    const stat = fs.lstatSync(path.join(__dirname, dir, file));
    if (stat.isDirectory()) {
      loadCommands(path.join(dir, file));
    } else {
      import(path.join(__dirname, dir, file))
        .then((f) => f.default)
        .then((command: Command) =>
          client.commands.set(command.data.name, command)
        );
      console.log(`Registered Command ${file}`);
    }
  }
};

export default {
  name: "ready",
  once: true,
  execute: (client: MyClient) => {
    loadCommands("../../commands");

    if (client.user) {
      client.user.setActivity(`Sussy Baka`, {
        type: "STREAMING",
        url: "https://www.youtube.com/watch?v=dQw4w9WgXcQ",
      });

      //connect to lavalink node
      client.manager?.init(client.user.id);
    }
    console.log("Bot is ready");
  },
} as Event;
