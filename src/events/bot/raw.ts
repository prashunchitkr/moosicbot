import { client } from "../..";
import { Event } from "../../types/common";

export default {
  name: "raw",
  execute: (d: any) => {
    client.manager!.updateVoiceState(d);
  },
} as Event;
