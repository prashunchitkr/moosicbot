import {
  GuildChannel,
  Permissions,
  VoiceChannel,
  VoiceState,
} from "discord.js";
import { client } from "../..";
import { Event } from "../../types/common";

export default {
  name: "voiceStateUpdate",
  execute: (oldState: VoiceState, newState: VoiceState) => {
    /* Leave when noone in voice channel */
    if (oldState.channelId && !newState.channelId) {
      const player = client.manager!.players.get(newState.guild.id);

      if (
        player &&
        (!oldState.channel?.members ||
          oldState.channel.members.size === 0 ||
          oldState.channel.members.filter(
            (mem) => !mem.user.bot && !mem.voice.deaf && !mem.voice.selfDeaf
          ).size < 1)
      ) {
        setTimeout(async () => {
          try {
            let vc = newState.guild.channels.cache.get(player.voiceChannel!);
            if (vc) vc = (await vc.fetch()) as GuildChannel;
            if (!vc)
              vc =
                ((await newState.guild.channels
                  .fetch(player.voiceChannel!)
                  .catch(() => null)) as GuildChannel) || false;
            if (!vc) return player.destroy();
            if (
              !vc.members ||
              vc.members.size === 0 ||
              vc.members.filter(
                (mem) => !mem.user.bot && !mem.voice.deaf && !mem.voice.selfDeaf
              ).size < 1
            ) {
              player.destroy();
              console.log(
                `Destroyed Player in ${
                  newState.guild && newState.guild.name
                    ? newState.guild.name
                    : player.guild
                }, because there were no listeners`
              );
            }
          } catch (e) {
            console.log(e);
          }
        }, 1000 * 60 * 5);
      }

      /* Deaf self when joinining a voice channel */
      if (
        newState.id == client.user!.id &&
        newState.channelId != oldState.channelId &&
        !newState.guild.me?.voice.deaf
      ) {
        if (
          newState.guild.me?.permissions.has(
            Permissions.FLAGS.DEAFEN_MEMBERS
          ) ||
          (newState.channel &&
            newState.channel
              .permissionsFor(newState.guild.me!)
              .has(Permissions.FLAGS.DEAFEN_MEMBERS))
        ) {
          newState.setDeaf(true).catch(() => {
            console.error("Failed to deafen self");
          });
        }
      }
      /* if (channel.members.size == 1) {
        setTimeout(() => {
          if (oldState.guild.me?.voice.channel) {
            if (oldState.guild.me.voice.channel.members.size === 1) {
              if (player) {
                player.destroy();
              }
              oldState.guild.me?.voice.disconnect();
            }
          }
        }, 1000 * 60 * 5);
      } */
    }
  },
} as Event;
