import { Event } from "../../types/common";
import { errorLogger } from "../../utils/logger";

export default {
  name: "error",
  execute: async (error: Error) => {
    // console.error(error);

    errorLogger.error(error);
  },
} as Event;
