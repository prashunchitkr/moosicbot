import { GuildMember, Interaction, VoiceChannel } from "discord.js";
import { client } from "../..";
import { Event } from "../../types/common";
import { errorLogger } from "../../utils/logger";

export default {
  name: "interactionCreate",
  execute: async (interaction: Interaction) => {
    if (!interaction.isCommand()) return;
    if (!interaction.inGuild()) return;

    const command = client.commands.get(interaction.commandName);

    if (!command) return;

    //TODO: ADD PERMISSION HANDLER

    if (command.requireDJ) {
      const djRole = interaction.guild?.roles.cache.find(
        (role) => role.name.toLowerCase() === "dj"
      );
      if (
        djRole &&
        !(interaction.member as GuildMember).roles.cache.get(djRole.id)
      ) {
        const member = client.guilds.cache
          .get(interaction.guildId)
          ?.members.cache.get(interaction.member.user.id);
        if (member) {
          const voiceChannel = member.voice.channel as VoiceChannel;

          if (voiceChannel && voiceChannel.members.size > 2) {
            return await interaction.reply({
              content: "You need `DJ` role to run this command",
              ephemeral: true,
            });
          }
        }
      }
    }

    try {
      await command.execute(client, interaction);
    } catch (error) {
      errorLogger.error(error);
      return await interaction.reply({
        content: "There was an error executing the command!",
        ephemeral: true,
      });
    }
  },
} as Event;
