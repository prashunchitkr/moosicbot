import { SlashCommandBuilder } from "@discordjs/builders";
import { Command } from "../../types/common";

export default {
  data: new SlashCommandBuilder()
    .setName("ping")
    .setDescription("Replies with pong"),

  execute: async (client, interaction): Promise<void> => {
    if (!interaction.isCommand()) return;

    await interaction.reply({
      content: `Pong! Took \`${Date.now() - interaction.createdTimestamp}ms\``,
    });
  },
} as Command;
