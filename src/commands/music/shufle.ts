import { SlashCommandBuilder } from "@discordjs/builders";
import { Command } from "../../types/common";

export default {
  requireDJ: true,
  data: new SlashCommandBuilder()
    .setName("shuffle")
    .setDescription("shuffle the current queue"),

  execute: async (client, interaction) => {
    const member = interaction.guild?.members.cache.get(interaction.user.id)!;

    if (!member.voice.channelId) {
      return interaction.reply({
        content: "You should be connected to a voice channel",
        ephemeral: true,
      });
    }

    const player = client.manager!.get(interaction.guildId);

    if (!player) {
      return await interaction.reply({
        content: "Not playing anything at the moment",
        ephemeral: true,
      });
    }

    if (player.voiceChannel !== member.voice.channelId) {
      return interaction.reply({
        content:
          "You should be connected in the voice channel same as bot to run this command",
        ephemeral: true,
      });
    }

    player.queue.shuffle();

    return await interaction.reply(":thumbsup: Shuffled");
  },
} as Command;
