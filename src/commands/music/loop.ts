import { SlashCommandBuilder } from "@discordjs/builders";
import { MessageEmbed } from "discord.js";
import { Command } from "../../types/common";

export default {
  requireDJ: true,
  data: new SlashCommandBuilder()
    .setName("loop")
    .setDescription("Loop the queue or a song")
    .addStringOption((o) =>
      o
        .setName("choice")
        .setDescription("choice to loop")
        .addChoices([
          ["track", "track"],
          ["queue", "queue"],
          ["none", "none"],
        ])
        .setRequired(true)
    ),
  execute: async (client, interaction) => {
    const loopOption = interaction.options.getString("choice", true);
    const member = interaction.guild?.members.cache.get(interaction.user.id)!;

    if (!member.voice.channelId) {
      return interaction.reply({
        content: "You should be connected to a voice channel",
        ephemeral: true,
      });
    }

    const player = client.manager!.get(interaction.guildId);

    if (!player) {
      return await interaction.reply({
        content: "Not playing anything at the moment",
        ephemeral: true,
      });
    }

    if (player.voiceChannel !== member.voice.channelId) {
      return interaction.reply({
        content:
          "You should be connected in the voice channel same as bot to run this command",
        ephemeral: true,
      });
    }

    switch (loopOption) {
      case "track":
        player.setTrackRepeat(true);
        break;

      case "queue":
        player.setQueueRepeat(true);
        break;

      case "none":
        player.setTrackRepeat(false);
        player.setQueueRepeat(false);
        break;

      default:
        break;
    }

    return await interaction.reply({
      embeds: [
        new MessageEmbed().setDescription(`Now repeating ${loopOption}`),
      ],
    });
  },
} as Command;
