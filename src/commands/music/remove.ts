import { SlashCommandBuilder } from "@discordjs/builders";
import { MessageEmbed } from "discord.js";
import { Command } from "../../types/common";

export default {
  requireDJ: true,
  data: new SlashCommandBuilder()
    .setName("remove")
    .setDescription("remove a track from the queue")
    .addNumberOption((o) =>
      o
        .setName("pos")
        .setDescription("position of track remove")
        .setRequired(true)
    ),

  execute: async (client, interaction) => {
    const pos = interaction.options.getNumber("pos", true);

    const member = interaction.guild?.members.cache.get(interaction.user.id)!;

    if (!member.voice.channelId) {
      return interaction.reply({
        content: "You should be connected to a voice channel",
        ephemeral: true,
      });
    }

    const player = client.manager!.get(interaction.guildId);

    if (!player) {
      return await interaction.reply({
        content: "Not playing anything at the moment",
        ephemeral: true,
      });
    }

    if (player.voiceChannel !== member.voice.channelId) {
      return interaction.reply({
        content:
          "You should be connected in the voice channel same as bot to run this command",
        ephemeral: true,
      });
    }

    if (pos < 1 || pos > player.queue.length + 1) {
      return interaction.reply({
        content: "Invalid track number",
        ephemeral: true,
      });
    }

    const temp = player.queue[pos - 1];
    player.queue.remove(pos - 1);

    const embed = new MessageEmbed().setDescription(
      `Removed track [${temp.title}](${temp.uri}) [${temp.requester}] from the queue`
    );

    return await interaction.reply({
      embeds: [embed],
    });
  },
} as Command;
