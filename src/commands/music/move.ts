import { SlashCommandBuilder } from "@discordjs/builders";
import { MessageEmbed } from "discord.js";
import { Command } from "../../types/common";

export default {
  requireDJ: true,
  data: new SlashCommandBuilder()
    .setName("move")
    .setDescription("move the position of track")
    .addNumberOption((o) =>
      o
        .setName("from")
        .setDescription("current position of track to move")
        .setRequired(true)
    )
    .addNumberOption((o) =>
      o
        .setName("to")
        .setDescription("desired position of track to move")
        .setRequired(true)
    ),

  execute: async (client, interaction) => {
    const from = interaction.options.getNumber("from", true);
    const to = interaction.options.getNumber("to", true);

    const member = interaction.guild?.members.cache.get(interaction.user.id)!;

    if (!member.voice.channelId) {
      return interaction.reply({
        content: "You should be connected to a voice channel",
        ephemeral: true,
      });
    }

    const player = client.manager!.get(interaction.guildId);

    if (!player) {
      return await interaction.reply({
        content: "Not playing anything at the moment",
        ephemeral: true,
      });
    }

    if (player.voiceChannel !== member.voice.channelId) {
      return interaction.reply({
        content:
          "You should be connected in the voice channel same as bot to run this command",
        ephemeral: true,
      });
    }

    if (
      from < 1 ||
      from > player.queue.length + 1 ||
      to < 1 ||
      to > player.queue.length
    ) {
      return interaction.reply({
        content: "Invalid track index",
        ephemeral: true,
      });
    }

    const temp = player.queue[from - 1];
    player.queue.add(temp, to - 1);
    player.queue.remove(from);

    const embed = new MessageEmbed().setDescription(
      `Moved [${temp.title}](${temp.uri}) to position \`${to}\``
    );

    return await interaction.reply({
      embeds: [embed],
    });
  },
} as Command;
