import { SlashCommandBuilder } from "@discordjs/builders";
import { EmbedField, GuildMember, MessageEmbed } from "discord.js";
import { Command } from "../../types/common";
import { msToTime } from "../../utils";

export default {
  data: new SlashCommandBuilder()
    .setName("now-playing")
    .setDescription("Show now playing track"),
  execute: async (client, interaction) => {
    const member = interaction.guild?.members.cache.get(interaction.user.id)!;

    const player = client.manager?.get(interaction.guildId);

    if (!player || !(player.playing || player.paused)) {
      return interaction.reply({
        content: "Not playing anything at the moment",
        ephemeral: true,
      });
    }

    const current_track = player.queue.current!;

    const fields: EmbedField[] = [
      {
        name: "Requested by",
        value: `${current_track.requester}`,
        inline: true,
      },
      {
        name: "Author",
        value: current_track.author!,
        inline: true,
      },
      {
        name: "Duration",
        value: `${msToTime(player.position)}/${msToTime(
          current_track.duration!
        )}`,
        inline: true,
      },
    ];

    const embed = new MessageEmbed()
      .setDescription(`**[${current_track.title}](${current_track.uri})**`)
      .addFields(fields);

    interaction.reply({
      embeds: [embed],
    });
  },
} as Command;
