import { SlashCommandBuilder } from "@discordjs/builders";
import { Command } from "../../types/common";

export default {
  requireDJ: true,
  data: new SlashCommandBuilder()
    .setName("skip")
    .setDescription("Skip current song"),
  execute: async (client, interaction) => {
    const member = interaction.guild?.members.cache.get(interaction.user.id)!;

    if (!member.voice.channelId) {
      return await interaction.reply({
        content: "You should be connected to a voice channel first.",
        ephemeral: true,
      });
    }

    const player = client.manager?.get(interaction.guildId);

    if (!player || !player.playing) {
      return interaction.reply({
        content: "Not playing anything at the moment",
        ephemeral: true,
      });
    }

    player.stop();
    interaction.reply({
      content: ":fast_forward: Skipping",
    });
  },
} as Command;
