import { SlashCommandBuilder } from "@discordjs/builders";
import { MessageButton, MessageEmbed } from "discord.js";
import { Command } from "../../types/common";
// @ts-ignore
import paginationEmbed from "discordjs-button-pagination";
import { msToTime } from "../../utils";

export default {
  data: new SlashCommandBuilder()
    .setName("queue")
    .setDescription("shows the current queue"),

  execute: async (client, interaction) => {
    const member = interaction.guild?.members.cache.get(interaction.user.id)!;

    const player = client.manager!.get(interaction.guildId);

    if (!player || !(player.playing || player.paused)) {
      return await interaction.reply({
        content: "Not playing anything at the moment",
        ephemeral: true,
      });
    }

    //pagination logic
    const tracksPerPage = 10;
    const pages = [];
    for (let i = 0; i < player.queue.length; i += tracksPerPage) {
      const pageEmbed = new MessageEmbed().setTitle("Upcoming Tracks");

      let list = "";
      let j = 1;
      for (const track of player.queue.slice(i, i + tracksPerPage)) {
        list += `\`${i + j}\`${track.requester}\`${msToTime(
          track.duration!
        )}\` __${track.title}__\n`;
        j += 1;
      }
      pageEmbed.setDescription(`\n${list}`);

      pageEmbed.addField(
        "Curently Playing",
        `**[${player.queue.current?.title}](${player.queue.current?.uri}) [${player.queue.current?.requester}]**`,
        false
      );

      pageEmbed.addField("Entries", player.queue.length.toString(), true);
      pageEmbed.addField(
        "Total Duration",
        msToTime(player.queue.duration),
        true
      );
      pageEmbed.addField(
        "Repeating",
        player.trackRepeat ? "Track" : player.queueRepeat ? "Queue" : "None",
        true
      );

      pages.push(pageEmbed);
    }

    if (!pages.length) {
      const pageEmbed = new MessageEmbed();
      pageEmbed.addField("Upcoming Tracks", "None", false);

      pageEmbed.addField(
        "Curently Playing",
        `**[${player.queue.current?.title}](${player.queue.current?.uri}) [${player.queue.current?.requester}]**`,
        false
      );

      pageEmbed.addField("Entries", player.queue.length.toString(), true);

      pageEmbed.addField(
        "Total Duration",
        msToTime(player.queue.duration),
        true
      );

      pageEmbed.addField(
        "Repeating",
        player.trackRepeat ? "Track" : player.queueRepeat ? "Queue" : "None",
        true
      );
      pages.push(pageEmbed);
    }

    const button1 = new MessageButton()
      .setCustomId("previousbtn")
      .setLabel("Previous")
      .setStyle("DANGER");

    const button2 = new MessageButton()
      .setCustomId("nextbtn")
      .setLabel("Next")
      .setStyle("SUCCESS");

    const buttons = [button1, button2];

    paginationEmbed(interaction, pages, buttons, 1000 * 60 * 1);
  },
} as Command;
