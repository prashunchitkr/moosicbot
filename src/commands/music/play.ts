import { SlashCommandBuilder } from "@discordjs/builders";
import { MessageEmbed } from "discord.js";
import { SearchResult } from "erela.js";
import { Command } from "../../types/common";

export default {
  data: new SlashCommandBuilder()
    .setName("play")
    .setDescription("Plays a song")
    .addStringOption((o) =>
      o
        .setName("data")
        .setDescription("name or url of the song to play")
        .setRequired(true)
    ),

  execute: async (client, interaction) => {
    const member = interaction.guild?.members.cache.get(interaction.user.id)!;

    if (!member.voice.channelId) {
      return await interaction.reply({
        content: "You should be connected to a voice channel first",
        ephemeral: true,
      });
    }

    const player =
      client.manager!.players.get(interaction.guildId) ||
      client.manager!.create({
        guild: interaction.guild!.id,
        voiceChannel: member.voice.channelId,
        textChannel: interaction.channelId,
      });

    if (player.state !== "CONNECTED") {
      player?.connect();
    } else if (member.voice.channelId !== player.voiceChannel) {
      return await interaction.reply({
        content:
          "You should be connected in the voice channel same as bot to run this command",
        ephemeral: true,
      });
    }

    await interaction.deferReply();

    let res: SearchResult;
    try {
      const query = interaction.options.getString("data", true);
      /* const spotURLRegexp =
        /^https:\/\/open.spotify.com\/(track|album|playlist)\/[0-9a-zA-Z]+(\?.*)?$/; */

      res = await client.manager!.search(query, member)!;

      if (res.loadType === "NO_MATCHES" || res.loadType === "LOAD_FAILED") {
        if (!player.queue.current) {
          player.destroy();
          throw { message: res.loadType };
        }
      }

      //resolving spotify tracks
      // if (spotURLRegexp.test(query)) {
      //   await interaction.followUp({
      //     content:
      //       "Loading metadata. This might take some time depending on playlist length...",
      //   });

      //   for (let i = 0; i < res.tracks.length; i++) {
      //     await (res.tracks[i] as UnresolvedTrack).resolve();
      //   }
      // }
    } catch (e) {
      await interaction.followUp({
        content: "Track(s) not found",
        ephemeral: true,
      });
      return await interaction.deleteReply();
    }

    const embed = new MessageEmbed();

    // for playlist
    if (res.loadType === "PLAYLIST_LOADED") {
      player?.queue.add(res!.tracks);
      embed.setDescription(`Queued **${res.tracks.length}** tracks`);
    } else if (
      res.loadType === "SEARCH_RESULT" ||
      res.loadType === "TRACK_LOADED"
    ) {
      // for single tracks
      player.queue.add(res!.tracks[0]);
      embed.setDescription(
        `Queued [${res?.tracks[0].title}](${res.tracks[0].uri}) [${interaction.member}]`
      );
    }

    if (!player.playing && !player.paused && !player.queue.size) player.play();

    if (
      !player.playing &&
      !player.paused &&
      player.queue.totalSize === res.tracks.length
    )
      player.play();

    return await interaction.editReply({
      content: ":white_check_mark: Done",
      embeds: [embed],
    });
  },
} as Command;
