import { SlashCommandBuilder } from "@discordjs/builders";
import { Command } from "../../types/common";

export default {
  requireDJ: true,
  data: new SlashCommandBuilder()
    .setName("resume")
    .setDescription("resumes paused the track"),

  execute: async (client, interaction) => {
    const member = interaction.guild?.members.cache.get(interaction.user.id)!;

    if (!member.voice.channelId) {
      return interaction.reply({
        content: "You should be connected to a voice channel",
        ephemeral: true,
      });
    }

    const player = client.manager!.get(interaction.guildId);

    if (!player) {
      return await interaction.reply({
        content: "Not playing anything at the moment",
        ephemeral: true,
      });
    }

    if (!player.paused) {
      return await interaction.reply({
        content: "Track not paused",
        ephemeral: true,
      });
    }

    if (player.voiceChannel !== member.voice.channelId) {
      return interaction.reply({
        content:
          "You should be connected in the voice channel same as bot to run this command",
        ephemeral: true,
      });
    }

    player.pause(false);

    return await interaction.reply(":play_pause: Resumed");
  },
} as Command;
