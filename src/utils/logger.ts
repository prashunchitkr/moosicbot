import log4js from "log4js";

log4js.configure({
  appenders: {
    errorFile: { type: "file", filename: "logs/bot.error.log" },
    console: { type: "console" },
  },
  categories: {
    default: {
      appenders: ["errorFile", "console"],
      level: "error",
    },
  },
});

const errorLogger = log4js.getLogger("errors");

export { errorLogger };
