import fs from "fs";
import path from "path";
import { REST } from "@discordjs/rest";
import { Routes } from "discord-api-types/v9";
import dotenv from "dotenv";
import { SlashCommandBuilder } from "@discordjs/builders";

dotenv.config();

const { CLIENTID, GUILDID, TOKEN } = process.env;

if (!CLIENTID || !GUILDID || !TOKEN) {
  throw new Error("Bot is not properly configured");
}

const commands: SlashCommandBuilder[] = [];

async function getCommands(dir: string) {
  const files = fs.readdirSync(path.join(__dirname, dir));

  for (const file of files) {
    const stat = fs.lstatSync(path.join(__dirname, dir, file));
    if (stat.isDirectory()) {
      await getCommands(path.join(dir, file));
    } else {
      const command = await import(path.join(__dirname, dir, file)).then(
        (f) => f.default
      );
      commands.push(command.data.toJSON());
    }
  }
}

const rest = new REST({ version: "9" }).setToken(TOKEN);

async function registerCommands(clientId: string, guildId: string) {
  await getCommands("./commands");
  try {
    if (process.env.NODE_ENV === "dev") {
      await rest.put(Routes.applicationGuildCommands(clientId, guildId), {
        body: commands,
      });
    } else {
      console.log("Registering Commands Globally");
      await rest.put(Routes.applicationCommands(clientId), {
        body: commands,
      });
    }
    console.log(`Finished registering ${commands.length} commands.`);
  } catch (error) {
    console.log(error);
  }
}

async function deleteCommands(clientId: string, guildId: string) {
  // Error can be ignored. Works somwhow.
  rest
    .get(Routes.applicationGuildCommands(clientId, guildId))
    .then((data: any) => {
      const promises = [];
      for (const command of data) {
        const deleteUrl = `${Routes.applicationGuildCommands(
          clientId,
          guildId
        )}/${command.id}`;
        promises.push(rest.delete(deleteUrl as `/${string}`));
      }
      return Promise.all(promises);
    })
    .then((commands) =>
      rest.put(Routes.applicationGuildCommands(clientId, guildId), {
        body: commands,
      })
    );
}

registerCommands(CLIENTID, GUILDID);
// deleteCommands(CLIENTID, GUILDID);
